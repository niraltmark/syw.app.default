﻿using System.Web;
using CommonGround;
using SYW.App.Default.Web.Config;

namespace SYW.App.Default.Web
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : HttpApplication
	{
		private ICommonContainer _container;

		protected void Application_Start()
		{
			Initialize();
		}

		private void Initialize()
		{
			_container = new CommonContainer();
			var bootstrapper = new Bootstrapper(_container);
			bootstrapper.RegisterEverything();
		}

		protected void Application_EndRequest()
		{
			// MiniProfiler.Stop();
		}
	}
}