﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Default.Domain.Installer;
using SYW.App.Default.Web.Web.Filters;

namespace SYW.App.Default.Web.Controllers
{
	public class HomePageController : MainControllerBase
	{
		private readonly IAppInstaller _appInstaller;

		public HomePageController(IAppInstaller appInstaller)
		{
			_appInstaller = appInstaller;
		}

		[PatternRoute("/")]
		public ActionResult Index()
		{
			return Content("The Price Tracker");
		}

		[IgnoreAutoLogin]
		[PatternRoute("/post-login")]
		public ActionResult PostLogin()
		{
			_appInstaller.Install();

			return Content("Yes");
		}
	}
}