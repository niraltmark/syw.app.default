﻿using CommonGround.Settings;

namespace SYW.App.Default.Domain.Services.Localization
{
	public interface ILocalizationSettings
	{
		[Default("messages_locale_tz")]
		string TimeZoneCookieName { get; set; }

		[Default("messages_locale_dl")]
		string DefaultLanguageCookieName { get; set; }
	}
}