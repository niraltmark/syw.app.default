using System;

namespace SYW.App.Default.Domain.Services.Platform
{
    public interface IPlatformConfiguration
    {
         string AppSecret { get; }
         long AppId { get; }
         Uri PlatformApiBaseUrl { get; }
		 Uri PlatformSecureApiBaseUrl { get; }
    }
}